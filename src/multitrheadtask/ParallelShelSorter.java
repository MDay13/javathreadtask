/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multitrheadtask;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MDay
 */
public class ParallelShelSorter 
{
    static private volatile Integer[] arr;

    public static synchronized Integer[] getArr() 
    {
        return arr;
    }

    public static synchronized void setArr(Integer[] arr) 
    {
        ParallelShelSorter.arr = arr;
    }
    
    public static synchronized void setArrEl(int index, int el) 
    {
        ParallelShelSorter.arr[index] = el;
    }
    
    public static synchronized Integer getArrEl(int index) 
    {
        return arr[index];
    }
    
    static public int [][] seq(int array[], int size) 
    {
        int countOfSeq = array.length/(array.length/size);
        int countElInSeq = array.length/size;
        int[][] seq = new int[countOfSeq][countElInSeq];
        for (int i=0; i<array.length; i++)
        {
                seq[i%size][i/size]=array[i];
        }
        
        return seq;
    }
    
    public static int[] shellSort (int array[], int step) throws InterruptedException
    {
        do 
        {
            ParallelInsertSorter[] pis = new ParallelInsertSorter[step];
            
            for (int i=0; i<step; i++)
            {
                int[][] seq = seq(array, step);
                pis[i].setArray(seq[i]);
                pis[i].start();
                pis[i].wait();
                pis[i].getArray();
            }
            
            step /= 2;
        }
        while (step/2 != 0);
        
       return null; 
    }
    
    public static Integer[] shellSort_1() throws InterruptedException
    {
            int j;
            int step = arr.length / 2;
            while (step > 0)
            {
                List<Thread> threads = new ArrayList<>();
                
                for (int i = 0; i < (arr.length - step); i++)
                {
                    Compare com = new Compare(i,step);
                    threads.add(com);
                    com.start();
                }
                for (Thread thread : threads)
                    thread.join();
                
                step = step / 2;
            }
            
            return arr;
    }
    
    public static class Compare extends Thread
    {
        private int j,step;
        
        public Compare (int j, int step)
        {
            this.step=step;
            this.j=j;
        }
        
        @Override
        public void run()
        {
            while ((j >= 0) && (arr[j] > arr[j + step]))
            {
                int tmp = getArrEl(j);
                setArrEl(j, getArrEl(j + step));
                setArrEl(j + step, tmp);        
                //new Compare(j, j+step).start();
                j-=step;
            }
        }
    }
}
