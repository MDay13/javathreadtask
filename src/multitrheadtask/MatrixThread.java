/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multitrheadtask;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MDay
 */
public class MatrixThread extends Thread
{
    private volatile Double[] raw;
    private volatile Double[] column;
    
    private volatile Double result;
    
    
    public MatrixThread(Double[] raw, Double[] column)
    {
        this.raw = raw;
        this.column = column;
        
        result = new Double(0);
    }

    @Override
    public void run() 
    {
        for (int i=0; i < raw.length;i++) 
        {
                result += raw[i]*column[i];
        }
    }

    public Double getResult() 
    {
        return result;
    }
    
}
