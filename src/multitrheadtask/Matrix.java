/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multitrheadtask;

/**
 *
 * @author MDay
 */
public class Matrix 
{
    private Double[][] body;
    
    public Matrix(Double[][] body)
    {
        this.body = body;
    }

    public Double[][] getBody() 
    {
        return body;
    }

    public void setBody(Double[][] body) 
    {
        this.body = body;
    }
    
    public Double[] getRow (int num)
    {
        return body[num];
    }
    
    public Double[] getColumn (int num)
    {
        Double[] result = new Double[body.length];
        for(int i=0; i<body.length; i++)
        {
            result[i]=body[i][num];
        }
        
        return result;
    }
    
    public Matrix multiply(Matrix matrix) throws InterruptedException
    {
        if (body.length != matrix.getBody().length)
            return null;
        
        Double[][] result = new Double [body.length][body.length];
        
        for (int i=0; i<body.length; i++)
        {
            for (int j=0; j<body.length; j++)
            {
                MatrixThread mt = new MatrixThread(matrix.getRow(i),matrix.getColumn(j));
                mt.start();
                mt.join();
                result[i][j] = mt.getResult();
            }
        }
        
        return new Matrix(result);
    }
    
    
}
