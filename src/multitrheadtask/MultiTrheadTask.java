/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multitrheadtask;

import java.util.Arrays;

/**
 *
 * @author MDay
 */
public class MultiTrheadTask {

    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws InterruptedException 
    {
        // TODO code application logic here
        Double[][] matrixBody = new Double [2][2];
        
        matrixBody[0][0] = 1d;
        matrixBody[0][1] = 2d;
        matrixBody[1][0] = 3d;
        matrixBody[1][1] = 4d;
        Matrix matrix = new Matrix(matrixBody);
        
        System.out.println("Matrix:" + Arrays.deepToString(matrixBody));
        
        System.out.println("Finish " + Arrays.deepToString(matrix.multiply(matrix).getBody()));
        
        Integer[] d = { 1, 12, 10, 2, 57, 145, 36, 911 };
        
        System.out.println(Arrays.toString(d));
        
        ParallelShelSorter.setArr(d);
        d = ParallelShelSorter.shellSort_1();
        System.out.println(Arrays.toString(d));
    }
    
}
